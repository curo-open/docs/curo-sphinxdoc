# Configuration file for the Sphinx documentation builder.
#
# Full list of options: http://www.sphinx-doc.org/en/master/config

# -- Use shared config
import os
import sys
sys.path.append(os.path.abspath('./_config'))
from sphinxconf import *

# -- Project information -----------------------------------------------------

project = 'Project name'
language = 'en'

# Version info for the project, acts as replacement for |version| and |release|
# The short X.Y version
# version = 'latest'
# The full version, including alpha/beta/rc tags
# release = 'latest'
