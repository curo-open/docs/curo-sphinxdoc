# -- Use shared config
import os
import sys
# sys.path.append(os.path.abspath('./_config'))

from sphinxconf import *

# -- Project information -----------------------------------------------------

project = 'Curo SphinxDoc shared config'
language = 'en'
html_title = 'Curo SphinxDoc shared config'
master_doc = 'README'

# Version info for the project, acts as replacement for |version| and |release|
# The short X.Y version
# version = 'latest'
# The full version, including alpha/beta/rc tags
# release = 'latest'