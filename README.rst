Shared Sphinx documentation config 
==================================

.. contents::
   :depth: 1
   :local:
   :backlinks: none

.. highlight:: console

This repository is sharable between Sphinx documentation projects.
Starting new Sphinx documentation is quick, but to get it fine tuned needs a lot more. 
This project should make it as simple as possible for our common needs. 

Preparation
-----------

Arch Linux
++++++++++++

You need to install following:

::

   $ yay -S python-sphinx python-sphinx_rtd_theme python-sphinx-autobuild python-docutils python-recommonmark python-sphinxcontrib-plantuml texlive-latexextra ttf-freefont freefonts

When using Visual Code for writting you should install also:

::

   $ yay -S python-doc8 docutils hunspell-sk hunspell-en_GB 
   $ ln -s /usr/share/hunspell ~/.config/Code/Dictionaries

MacOS
+++++

::

   $ brew install elvish 
   $ brew install python
   $ brew cask install adoptopenjdk
   $ brew install plantuml
   $ sudo pip install Sphinx sphinx_rtd_theme sphinx-autobuild docutils recommonmark sphinxcontrib-plantuml 
   /usr/local/bin/python3 -m pip install -U Sphinx sphinx_rtd_theme sphinx-autobuild docutils recommonmark sphinxcontrib-plantuml --user

Create new sphinx project
-------------------------

::

   $ mkdir prj
   $ cd prj
   $ git init
   $ git submodule add https://gitlab.com/curo-open/docs/curo-sphinxdoc.git _config
   $ ./_config/setup.sh

Upgrade curo-sphinxdoc in your project
--------------------------------------

::

   $ cd prj
   $ git submodule update --remote --merge

Build documentation
-------------------

::

   # live reload
   $ make livehtml
   # + open http://127.0.0.1:8000/ in Browser

   # generate site
   $ make html
   # generate PDF
   $ make latexpdf
   # generate epub
   $ make epub

Learn to write documentation
----------------------------

The `full Sphinx documentation`__ is good read and use as reference. 
Source of Sphinx documentation is also the best place to see real `life examples`__.
The `readthedocs.io`__ project contains also huge amound of great information.

You need also tutorial and `reference`__ when using the great PlantUML tool to write diagrams.

I found inspiration of how to write the shared configuration in `rdt conf.py`__

__ http://www.sphinx-doc.org/en/master/contents.html
__ https://github.com/sphinx-doc/sphinx/tree/master/doc
__ https://docs.readthedocs.io/en/latest/
__ http://plantuml.com/use-case-diagram
__ https://github.com/rtfd/rtd-sphinx-themes-examples/blob/master/docs/conf.py 
