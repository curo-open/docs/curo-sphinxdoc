#!/bin/sh

git config core.hooksPath ./_config/hooks
mkdir -p .vscode
ln -s ../_config/files/extensions.json .vscode/
ln -s _config/files/root/Makefile ./
ln -s _config/files/root/make.bat ./
if [ ! -f ./requirements.txt ]; then
    cp _config/files/root/requirements.txt ./
fi
if [ ! -f ./conf.py ]; then
    cp _config/files/conf.py ./
fi
