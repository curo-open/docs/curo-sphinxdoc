#!/bin/sh

git config core.hooksPath ./_config/hooks
mkdir -f .vscode
ln -s _config/files/extensions.json .vscode/
ln -s _config/files/root/* ./
ln -s _config/files/root/.gitignore ./
if [ ! -f /tmp/foo.txt ]; then
    cp _config/files/conf.py ./
fi
