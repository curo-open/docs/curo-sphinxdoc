#!/bin/sh
die () {
    echo >&2 "$@"
    exit 1
}

[ "$#" -eq 1 ] || die "1 argument required, $# provided"
TARGET="$1"
mkdir -p "$TARGET"

# generate files
make html
make latexpdf
make epub

# copy to KeyBase
rsync -a --delete --exclude '*.epub' --exclude '*.pdf' _build/html/ "$TARGET"
rsync -a --include='*.epub' --exclude='*' _build/epub/ "$TARGET"
rsync -a --include='*.pdf' --exclude='*' _build/latex/ "$TARGET"

#echo "[error]  Blocking commit, found errors while building documentation:"
#exit 1

echo "[success]  Documentation published."